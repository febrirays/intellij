package com.hackathon.rocky.dao;

import com.hackathon.rocky.entity.ContractTransaction;
//import com.hackathon.rocky.entity.CouponTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface ContractTransactionRepository extends JpaRepository<ContractTransaction,String> {

    @Query(value = "SELECT * FROM contract_transaction WHERE user_id LIKE:userId",nativeQuery = true)
    List<ContractTransaction> findTransactionByUserId(@Param("userId") String userId);

}
