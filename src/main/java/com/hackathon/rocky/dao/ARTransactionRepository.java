package com.hackathon.rocky.dao;

import com.hackathon.rocky.entity.ARTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository
public interface ARTransactionRepository extends JpaRepository<ARTransaction, String> {
    @Query(value = "SELECT * FROM ar_transaction WHERE user_id LIKE :userId", nativeQuery = true)
    List<ARTransaction> findTransactionByUserId(@Param("userId") String userId);

    @Query(value = "SELECT * FROM ar_transaction WHERE location_id LIKE :locationId", nativeQuery = true)
    ARTransaction checkArTransactionLocationIfAlreadyExists(@Param("locationId") String locationId);

    @Query(value = "SELECT * FROM ar_transaction WHERE user_id LIKE :userId AND location_id LIKE :locationId", nativeQuery = true)
    List<ARTransaction> getArTransactionByUserIdAndLocationId(@Param("userId") String userId, @Param("locationId") String locationId);
}
