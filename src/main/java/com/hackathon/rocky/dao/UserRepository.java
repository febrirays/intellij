package com.hackathon.rocky.dao;

import com.hackathon.rocky.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Transactional
@Repository
public interface UserRepository extends JpaRepository<User,String> {
    @Query(value = "SELECT * FROM users WHERE user_name LIKE:userId",nativeQuery = true)
    Optional<User> findByName(@Param ("userId") String username);

    @Query(value = "SELECT * FROM users WHERE user_name LIKE:userId",nativeQuery = true)
    User findByUserName(@Param ("userId") String username);

//    List<User> findUserById(@Param("userId") String username);
////    Optional<User> findByName(String username);
}
