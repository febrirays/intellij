package com.hackathon.rocky.dao;

import com.hackathon.rocky.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;


@Transactional
@Repository
public interface CouponTransactionRepository extends JpaRepository<Transaction,String> {

    //@Query(" SELECT u from Transaction c ")
    //@Query(" SELECT new com.hackathon.rocky.dto.CouponTransactionDto()"+
     //       "FROM Transaction c INNER JOIN c.user u where c.")
    //@Query(value = "select * from")

//    @Query = "hkgk";
//
//    List<Transaction> f(String userId);

//    @Query("Select c from Transaction c join c.user u where c.user = u")
//
//@Query(value = "SELECT * FROM coupon_transaction ct INNER JOIN user u on u.user_id = ct.user_id INNER JOIN coupon c on c.coupon_id - ct.coupon_id where ct.user_id LIKE :userId",nativeQuery = true)
//@Query(value = "SELECT * FROM coupon_transaction ct INNER JOIN user u on u.user_id = ct.user_id where ct.user_id LIKE :userId",nativeQuery = true)
@Query(value = "select * FROM `Transaction` WHERE user_id LIKE  :userId order by trans_time desc",nativeQuery = true)
List<Transaction> findTransactionByUserId(@Param("userId") String userId);





//@Query("select * FROM" +
//        "Transaction ct " +
//        "INNER JOIN user u on u.user_id = ct.user_id" +
//        "INNER JOIN coupon c on c.coupon_id = ct.coupon_id" +
//        " WHERE ct.user_id LIKE  :userId",nativeQuery = true")
//

}

