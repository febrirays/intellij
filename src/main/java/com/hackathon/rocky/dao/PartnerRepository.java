package com.hackathon.rocky.dao;


import com.hackathon.rocky.entity.Partner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional

public interface PartnerRepository extends JpaRepository<Partner,String> {
//    PartnerRepository findAllByPartner_address(String partneraddress);
}
