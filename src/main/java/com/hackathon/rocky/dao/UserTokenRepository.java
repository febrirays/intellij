package com.hackathon.rocky.dao;


import com.hackathon.rocky.entity.UserToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface UserTokenRepository extends JpaRepository<UserToken, String> {
    //List<UserToken> deleteByUserTokenIdAndUser_User_id(
    //        @Param("token") String tokenID, String userID);
    void deleteByUserUserIdAndTokenType(String userID, String tokenType);
    //void deleteUserTokenByUserUserIdAndTokenTypeAndTokenDateTimeNear(String userID, String tokenType, LocalDateTime localDateTime);
    UserToken findTopByUserUserIdAndTokenTypeOrderByTokenDateTimeAsc(
            String userID,String tokenType);
    UserToken findTopByUserUserIdOrderByTokenDateTimeAsc(String userID);

    Integer countUserTokensByUserUserIdAndTokenType(String userID, String tokenType);

    boolean existsByUserUserIdAndTokenType(String userID, String tokenType);
    boolean existsByUserUserId(String userID);

    List findAllByOrderByUser();
    List findAllByUserUserIdOrderByTokenDateTime(String userID);
}
