package com.hackathon.rocky.dao;

import com.hackathon.rocky.entity.Coupon;
import com.hackathon.rocky.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface CouponRepository extends JpaRepository<Coupon,String> {
//    Coupon getCouponByCoupon_id(String id);
@Query(value = "SELECT * FROM coupon WHERE coupon_id  NOT IN (SELECT coupon_id FROM `Transaction` WHERE user_id LIKE :userId " +
        "AND coupon_id IS NOT NULL)" +
        " order by coupon_id DESC ",nativeQuery = true)
List<Coupon> findTransactionByUserIdBought(@Param("userId") String userId);

@Query(value = "SELECT * FROM coupon WHERE coupon_id IN (SELECT coupon_id FROM `Transaction` WHERE user_id LIKE :userId " +
        "AND coupon_id IS NOT NULL)" +
        "order by coupon_id DESC ",nativeQuery = true)
List<Coupon> findTransactionByUserIdNotBought(@Param("userId") String userId);


}
