package com.hackathon.rocky.controller;

import com.hackathon.rocky.dao.UserRepository;
import com.hackathon.rocky.entity.User;
import com.hackathon.rocky.security.JWTGenerator;
import com.hackathon.rocky.service.Impl.CustomUserDetailsService;
import com.hackathon.rocky.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private CustomUserDetailsService customUserDetailsService;
    private UserRepository userRepository;

    public static final long EXP_SECOND=60;

    private static final String KEY="HELLOWORLD";
    private JWTGenerator jwtGenerator;

    public UserController(JWTGenerator jwtGenerator) {
        this.jwtGenerator = jwtGenerator;
    }


    @CrossOrigin
    @GetMapping("/view/{userId}")
    public ResponseEntity<?> viewUser(@PathVariable String userId){


        User user1= userService.findUserbyId(userId);

        return new ResponseEntity(user1, HttpStatus.OK);
    }



}
