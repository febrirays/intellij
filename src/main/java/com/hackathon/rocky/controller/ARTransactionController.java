package com.hackathon.rocky.controller;

import com.hackathon.rocky.dto.ARTransactionDto;
import com.hackathon.rocky.dto.UserARHistoryDto;
import com.hackathon.rocky.entity.ARTransaction;
import com.hackathon.rocky.service.ARTransactionService;
import org.apache.tomcat.util.http.parser.HttpParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/ar-transaction")
public class ARTransactionController {

    @Autowired
    private ARTransactionService arTransactionService;

    @CrossOrigin
    @PostMapping("/save-ar-transaction")
    public ResponseEntity<?> saveARTransaction(@RequestBody ARTransactionDto arTransactionDto){
        List<ARTransaction> arTransactions = arTransactionService.getArTransactionByUserIdAndLocationId(arTransactionDto.getUserId(), arTransactionDto.getArLocationId());

        if(!arTransactions.equals(null)){
            for(ARTransaction transaction : arTransactions){
                if(LocalDateTime.now().getDayOfMonth() == transaction.getArTransactionTime().getDayOfMonth()){
                    return new ResponseEntity("This location had been scanned.", HttpStatus.OK);
                }else{
                    if(transaction.isNewest() == false)continue;
                    transaction.setNewest(false);
                    arTransactionService.updateARTransaction(transaction);

                    arTransactionService.saveARTransaction(arTransactionDto);
                    return new ResponseEntity(arTransactionDto, HttpStatus.OK);
                }
            }
        }
        arTransactionService.saveARTransaction(arTransactionDto);
        return new ResponseEntity(arTransactionDto, HttpStatus.OK);
    }

    @PostMapping("/update-ar-transaction")
    public ResponseEntity<?> updateARTransaction(@RequestBody ARTransaction arTransaction){
        arTransactionService.updateARTransaction(arTransaction);
        return new ResponseEntity(arTransaction, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/delete-ar-transaction/{arTransactionId}")
    public ResponseEntity<?> deleteContract(@PathVariable String arTransactionId){
        arTransactionService.deleteARTransaction(arTransactionId);
        return new ResponseEntity(arTransactionId, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/get-all-transaction")
    public ResponseEntity<?> getAllTransaction(){
        List<ARTransaction> arTransactions = arTransactionService.findAll();
        return new ResponseEntity(arTransactions, HttpStatus.OK);
    }

    /*@GetMapping("/get-transaction/{arTransactionId}")
    public ResponseEntity<?> getTransactionById(@PathVariable String arTransactionId){
        ARTransaction arTransaction = arTransactionService.findById(arTransactionId);
        return new ResponseEntity(arTransaction, HttpStatus.OK);
    }*/

    @CrossOrigin
    @GetMapping("/get-user-ar-transaction-history/{userId}")
    public ResponseEntity<?> getUserARTransactionHistory(@PathVariable String userId){
        List<UserARHistoryDto> userARHistoryDtos = arTransactionService.getUserHistory(userId);
        return new ResponseEntity(userARHistoryDtos, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/get-all-transaction/{userId}")
    public ResponseEntity<?> getAllTransactionByUserId(@PathVariable String userId){
        List<ARTransaction> arTransactions = arTransactionService.findAllByUserId(userId);
        return new ResponseEntity(arTransactions, HttpStatus.OK);
    }

    @GetMapping("/check-expiration-date/{userId}")
    public ResponseEntity<?> checkArTransactionDateByTransactionId(@PathVariable String userId){
        List<ARTransaction> arTransactions = arTransactionService.findAllByUserId(userId);
        return new ResponseEntity(arTransactions, HttpStatus.OK);
    }

}
