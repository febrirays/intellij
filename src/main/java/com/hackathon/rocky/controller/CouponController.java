package com.hackathon.rocky.controller;

import com.hackathon.rocky.dao.CouponRepository;
import com.hackathon.rocky.dto.CouponDto;
import com.hackathon.rocky.entity.Coupon;
import com.hackathon.rocky.exceptionHandler.ErrorMessageException;
import com.hackathon.rocky.service.CouponService;
import lombok.var;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping(value = "/coupon")
@Validated
public class CouponController {

    Logger log = LoggerFactory.getLogger(CouponController.class);

    @Autowired
    private CouponService couponService;



    //save
    @PostMapping("/save-coupon")
    @ResponseStatus(HttpStatus.CREATED)

    public ResponseEntity<?> saveCoupon(@Valid @RequestBody CouponDto couponDto){

        Coupon coupon = couponService.saveCoupon(couponDto);

        return new ResponseEntity(coupon, HttpStatus.OK);

    }



    //find 1
    @GetMapping("/get-coupon/{couponId}")
    public ResponseEntity<?> getCoupon(@Validated @NotBlank @PathVariable String couponId) {

        return new ResponseEntity(couponService.getcouponDTO(couponId), HttpStatus.OK);
    }


    //Get all coupon to show on homepage
    @CrossOrigin
    @GetMapping("/getAllCouponDto/{userId}")
    public ResponseEntity<?> getUserHistory(@PathVariable String userId) {

        List<CouponDto> allCoupon = couponService.getAllCouponDto(userId);

        return new ResponseEntity(allCoupon, HttpStatus.OK);
    }

    @GetMapping("/get-AllCoupons")
    public ResponseEntity<?> getCoupon() {
        //service

        List<Coupon> allData = couponService.getAllData();
        return new ResponseEntity(allData, HttpStatus.OK);
    }



    //TODO buat sistem supaya hanya bisa search yang dia punya DAN setelah login --> Authorization
        //get image stored as BLOB
//    @GetMapping("/get-couponBlob/{couponId}")
//    public ResponseEntity<?> couponImageBlob(@PathVariable String couponId) {
//        Coupon coupon = couponService.getCouponById(couponId);
//        if (coupon.getCoupon_picture() == null   ) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//           //return ResponseEntity.
//        }
//
//        return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "image/*").body(coupon.getCoupon_picture());
//   }



    //get image already rendered - DB
//    @GetMapping("/{couponid}/couponImage")
//    public void renderImageFromDB(@PathVariable String couponid, HttpServletResponse response) throws IOException {
//        Coupon coupon = couponService.getCouponById(couponid);
//
//        if (coupon.getCoupon_picture() != null) {
//            byte[] byteArray = new byte[coupon.getCoupon_picture().length];
//            int i = 0;
//
//            for (Byte wrappedByte : coupon.getCoupon_picture()){
//                byteArray[i++] = wrappedByte; //auto unboxing
//            }
//
//            response.setContentType("image/jpeg");
//            InputStream is = new ByteArrayInputStream(byteArray);
//            StreamUtils.copy(is, response.getOutputStream());
//        }
//    }


//    @GetMapping(
//            value = "/imagg",
//            produces = MediaType.IMAGE_JPEG_VALUE
//    )
//    public @ResponseBody byte[] getImageWithMediaType(@RequestParam String couponId) throws IOException {
////        Path currentPath = Paths.get(".");
////        Path absolutePath =  currentPath.toAbsolutePath();
//        Coupon coupon = couponService.getCouponById(couponId);
//        String str = coupon.getCoupon_gambar();
//        String arrstr[] = str.split("\\./",2);
//        String path = '/' + String.join(",", arrstr[1]);
//
//        InputStream in = getClass()
////               .getResourceAsStream( "/src/main/resources/image/jack.jpg");
//                .getResourceAsStream(path);
//        return StreamUtils.copyToByteArray(in);
//    }


    //Serve gambar ke front end
    @RequestMapping(value = "/gambar", method = RequestMethod.GET,
            produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<InputStreamResource> getImage(@RequestParam String couponId) throws IOException {


        Coupon coupon = couponService.getCouponById(couponId);
//        String str = coupon.getCoupon_gambar();
//        String arrstr[] = str.split("\\./",2);
//        String path = '/' + String.join(",", arrstr[1]);

        //belajar stream
        //String address = "D:\\assets/";
        //String fileName = "bmo.jpg";
        String address = coupon.getCoupon_gambar();
        Path path = Paths.get(Stream.of(address).collect(Collectors.joining("\\"))).toAbsolutePath().normalize();
//        Path path2 = Paths.get(Stream.of(address).collect(Collectors.joining()))
        var imgFile = new UrlResource(path.toUri());

        return ResponseEntity
                .ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(new InputStreamResource(imgFile.getInputStream()));
    }







    //update
    @PostMapping("/update-coupon")
    public ResponseEntity<?> updateCoupon(@Valid @RequestBody Coupon coupon){

        // do validation

        // save contract data
        couponService.updateCoupon(coupon);

        return new ResponseEntity(coupon, HttpStatus.OK);
    }



    //delete
    @GetMapping("/delete-contract/{couponId}")
    public ResponseEntity<?> deleteCoupon(@PathVariable String couponid){
        couponService.deleteCoupon(couponid);

        return new ResponseEntity(couponid, HttpStatus.OK);
    }





    //save image
//    @PostMapping("/save-image/{id}")
//    public String saveImage(@PathVariable String id, @RequestParam("imagefile") MultipartFile file){
//
//        couponService.saveImage(sString.valueOf(id), file);
//
//        return "success";
//
//    }

    @PostMapping("/uploadImage")
    public String uploadImage(@RequestParam("imageFile") MultipartFile imageFile, @RequestParam String couponId) throws Exception {

        //ModelAndView modelAndView = new ModelAndView();

        String returnvalue = "Success";
        try {
            couponService.uploadImages(imageFile,couponId);
        } catch (ErrorMessageException e) {
            //log.error("Unable to save image",e);
            e.printStackTrace();
            throw new ErrorMessageException("there's already image for coupon Id " + couponId);
                //modelAndView.setViewName("error");

        }

       // modelAndView.addObject()
        return returnvalue;

    }

    @PostMapping("/overwriteImage")
    public String overwriteImage(@RequestParam("imageFile") MultipartFile imageFile, @RequestParam String couponId) throws Exception {

        //ModelAndView modelAndView = new ModelAndView();

        String returnvalue = "Success";
        try {
            couponService.overwriteImages(imageFile,couponId);
        } catch (ErrorMessageException e) {
            //log.error("Unable to save image",e);
            e.printStackTrace();
            throw new ErrorMessageException(couponId);
            //modelAndView.setViewName("error");

        }

        // modelAndView.addObject()
        return returnvalue;

    }



}
