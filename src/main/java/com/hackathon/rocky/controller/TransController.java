package com.hackathon.rocky.controller;

import com.hackathon.rocky.dto.CouponRequest;
import com.hackathon.rocky.service.CouponTransactionService;
import com.hackathon.rocky.entity.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/Trans")
public class TransController {


    @Autowired
    private CouponTransactionService couponTransactionService;

    //TODO nerima post dari front end buat point


    @CrossOrigin
    @PostMapping("/save-couponTrans")
    public ResponseEntity<?> saveCouponTrans(@RequestBody CouponRequest couponTrans) {
        Transaction transaction = couponTransactionService.saveCouponTrans(couponTrans);

        return new ResponseEntity(transaction, HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping("/save-pointTrans")
    public ResponseEntity<?> savePointTrans(@RequestBody CouponRequest couponTrans) {

        Transaction transaction = couponTransactionService.savePoint(couponTrans);

        return new ResponseEntity(transaction, HttpStatus.OK);
    }


    //Ambil data transaksi kupon user
    @CrossOrigin
    @GetMapping("/get-userHistory/{userId}")
    public ResponseEntity<?> getUserHistory(@PathVariable String userId) {
        return new ResponseEntity(couponTransactionService.findAllTransactionByUserID(userId), HttpStatus.OK);
    }

    //Show ALL Transaction data
    @GetMapping("/get-allCouponTrans")
    public ResponseEntity<?> getCoupon() {
        //service

        List<Transaction> allData = couponTransactionService.getAllData();
        return new ResponseEntity(allData, HttpStatus.OK);
    }

    @GetMapping("/getOneTrans/{transId}")
    public ResponseEntity<?> getOneTrans(@PathVariable String transId) {
        //service


        return new ResponseEntity(couponTransactionService.getOneTransaction(transId), HttpStatus.OK);

    }




//     return new ResponseEntity(??, HttpStatus.OK);
//     return new ResponseEntity(??, HttpStatus.OK);


//    @GetMapping("/get-coupon/{CouponId}")
//    public ResponseEntity<?> getCoupon(@PathVariable String Couponid){
//
//        // do validation
//
//        Coupon coupon1 = new Coupon();
//        coupon1.getCoupon_name();
//        coupon1.getCouponType().getCoupon_type_name();
//        coupon1.getPartner().getPartner_name();
//
//        // save contract data
//
//        return  coupon1;
//    }

    @PostMapping("/update-couponTrans")
    public ResponseEntity<?> updateCouponTrans(@RequestBody Transaction couponTrans) {

        // do validation

        // save contract data
        couponTransactionService.updateCouponTrans(couponTrans);

        return new ResponseEntity(couponTrans, HttpStatus.OK);
    }

    @GetMapping("/post-contractTrans/")
    public ResponseEntity<?> simpan(@PathVariable String CouponId, @PathVariable String UserId) {

        return new ResponseEntity(CouponId, HttpStatus.OK);

    }

    @GetMapping("/delete-contractTrans/{CouponTransId}")
    public ResponseEntity<?> deleteCoupon(@PathVariable String CouponTransId) {
        couponTransactionService.deleteCouponTrans(CouponTransId);
        return new ResponseEntity(CouponTransId, HttpStatus.OK);
    }

}



//    @RequestMapping(value = "/get/{couponTransaction}", method = RequestMethod.GET)
//
//    public String showBookDetails(@PathVariable String couponTransaction, Model model) {
//
//        model.addAttribute("couponTransaction", couponTransaction);
//
//
//        return new CouponTransactionRepository(couponTransaction);