package com.hackathon.rocky.controller;


import com.hackathon.rocky.entity.Partner;
import com.hackathon.rocky.service.PartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/partner")
public class PartnerController {

    @Autowired
    private PartnerService partnerService;


    @PostMapping("/save-partner")
    public ResponseEntity<?> savePartner(@Valid @RequestBody Partner partner){

        // do validation

        Partner partner1= new Partner();
        partner1.setPartner_name("jonny sins");
        partner1.setPartner_description("24234234");

        // save contract data
        partnerService.savePartner(partner);

        return new ResponseEntity(partner, HttpStatus.OK);
    }

    @PostMapping("/update-partner")
    public ResponseEntity<?> updatePartner(@RequestBody Partner partner){

        // do validation

        // save contract data
        partnerService.updatePartner(partner);

        return new ResponseEntity(partner, HttpStatus.OK);
    }

    @GetMapping("/delete-contract/{PartnerId}")
    public ResponseEntity<?> deletePartner(@PathVariable String PartnerId){

        // do validation

        // save contract data
        partnerService.deletePartner(PartnerId);
        return new ResponseEntity(PartnerId, HttpStatus.OK);
    }


}
