package com.hackathon.rocky.controller;

import com.hackathon.rocky.entity.ARLocation;
import com.hackathon.rocky.service.ARLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.File;
import java.io.FileInputStream;

@RestController
@RequestMapping(value = "/api/v1/ar-location")
public class ARLocationController {

    @Autowired
    private ARLocationService arLocationService;

    @PostMapping("/save-ar-location")
    public ResponseEntity<?> saveARLocation(@Valid @RequestBody ARLocation arLocation){
        arLocationService.saveARLocation(arLocation);
        return new ResponseEntity(arLocation, HttpStatus.OK);
    }

    @PostMapping("/update-ar-location/{locationId}")
    public ResponseEntity<?> updateARLocation(@RequestBody ARLocation arLocation,@PathVariable String locationId){
        ARLocation newArLocation = arLocationService.getLocationById(locationId);

        newArLocation.setLocationName(arLocation.getLocationName());
        newArLocation.setLocationAddress(arLocation.getLocationAddress());/*
        newArLocation.setLocationPicture(arLocation.getLocationPicture());*/
        newArLocation.setLongitude(arLocation.getLongitude());
        newArLocation.setLatitude(arLocation.getLatitude());

        arLocationService.updateARLocation(newArLocation);
        return new ResponseEntity(arLocation, HttpStatus.OK);
    }

    @GetMapping("/delete-ar-location/{arLocationId}")
    public ResponseEntity<?> deleteARLocation(@PathVariable String arLocationId){
        arLocationService.deleteARLocation(arLocationId);
        return new ResponseEntity(arLocationId, HttpStatus.OK);
    }


}
