package com.hackathon.rocky.controller;


import com.hackathon.rocky.dto.ContractTransactionDto;
import com.hackathon.rocky.entity.ContractTransaction;
import com.hackathon.rocky.service.ContractTransService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/contract")
public class ContractTransController {

    @Autowired
    private ContractTransService contractTransService;

    @CrossOrigin
    @PostMapping("/add")
    public ResponseEntity<?> savecontract(@RequestBody ContractTransactionDto contractTransaction){
      contractTransService.savecontract(contractTransaction);
        return new ResponseEntity(contractTransaction, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/view/{userId}")
    public ResponseEntity view(@PathVariable String userId){

        List<ContractTransaction> contractTransactions= contractTransService.findAll(userId);
        return new ResponseEntity(contractTransactions,HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/view/history/{userId}")
    public ResponseEntity history(@PathVariable String userId){

        return new ResponseEntity(contractTransService.showhistory(userId),HttpStatus.OK);
    }
}
