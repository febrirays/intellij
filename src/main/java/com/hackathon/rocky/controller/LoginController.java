package com.hackathon.rocky.controller;

import com.hackathon.rocky.dto.JwtUser;
import com.hackathon.rocky.entity.User;
import com.hackathon.rocky.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
public class LoginController {

    @Autowired
    UserService userService;

    @CrossOrigin
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json")
    public String login(@RequestBody JwtUser jwtUser) throws IOException {


        return userService.login(jwtUser);
    }
    @CrossOrigin
    @PostMapping("/save-user")
    public ResponseEntity<?> saveUser(@RequestBody User user){


        userService.saveUser(user);

        return new ResponseEntity(user, HttpStatus.OK);
    }


}