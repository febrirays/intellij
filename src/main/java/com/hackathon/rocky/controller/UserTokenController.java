package com.hackathon.rocky.controller;


import com.hackathon.rocky.dto.UserToken.UserTokenDto;
import com.hackathon.rocky.entity.UserToken;
import com.hackathon.rocky.service.UserService;
import com.hackathon.rocky.service.UserTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "userToken")
public class UserTokenController {


    @Autowired
    private UserTokenService userTokenService;

    @CrossOrigin
    @ExceptionHandler
    @PostMapping("/saveToken")
    public ResponseEntity<?> saveUserToken(@RequestBody UserTokenDto userTokenDto)throws Exception{
        if(!userTokenService.userExistsByUserId(userTokenDto))
        {
            return new ResponseEntity("Sorry userID not found",
                    HttpStatus.NOT_FOUND);
        }
        else if(userTokenDto.getTokenType().equals("AR")||
                userTokenDto.getTokenType().equals("TR")) {
            userTokenService.saveUserToken(userTokenDto);
            return new ResponseEntity(userTokenService.countUserToken(userTokenDto), HttpStatus.OK);

        }
        return new ResponseEntity("Sorry tokenType must be 'AR' or 'TR'",
                HttpStatus.BAD_REQUEST);

        //return new ResponseEntity("error", HttpStatus.NOT_FOUND);
    }

    @CrossOrigin
    @DeleteMapping("/useToken/userTokenID")
    public ResponseEntity<?> deletebyUTID
            (@RequestBody UserToken userToken)
    {
        userTokenService.deleteByUTID(userToken.getUserTokenId());
        return new ResponseEntity(userToken.getUserTokenId(),HttpStatus.OK);
    }

    @CrossOrigin
    @DeleteMapping("/useToken/type")
    public ResponseEntity<?> deleteTokenByUserIDandtokenType
            (@RequestBody UserTokenDto userTokenDto)
    {
        if(userTokenService.tokenExistByUserIdAndTokenType(userTokenDto)) {
            return new ResponseEntity(userTokenService.deleteSpecToken(
                    userTokenDto), HttpStatus.OK);
        }
        return new ResponseEntity(
                "token associated with '"+
                        userTokenDto.getUserId()+"' and '"+
                        userTokenDto.getTokenType()+"' is not found",
                HttpStatus.NOT_FOUND);
    }

    @CrossOrigin
    @DeleteMapping("/useToken/allType")
    public ResponseEntity<?> deleteTokenByUserID(@RequestBody UserTokenDto userTokenDto)
    {
        if(userTokenService.tokenExistByUserId(userTokenDto))
        {
            return new ResponseEntity(userTokenService.deleteAllTypeToken(
                    userTokenDto), HttpStatus.OK);
        }
        return new ResponseEntity(
                "token associated with '"+
                        userTokenDto.getUserId()+"' and '"+
                        userTokenDto.getTokenType()+"' is not found"
                ,HttpStatus.NOT_FOUND);
    }

    @CrossOrigin
    @GetMapping("/find")
    public ResponseEntity<?> findOldestTokenByUserIDandtokenType
            (@RequestBody UserTokenDto userTokenDto)
    {
        UserToken chosenUserToken =
        userTokenService.findOldestUserTokenByUserIDandtokenType(userTokenDto);
        return new ResponseEntity(chosenUserToken,HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/findAll")
    public ResponseEntity<?> findAll(@RequestBody UserTokenDto userTokenDto)
    {
        return new ResponseEntity(userTokenService.getAllTokens(userTokenDto),HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/findAll/userID")
    public ResponseEntity<?> findAllbyUserID(@RequestBody UserTokenDto userTokenDto)
    {
        return new ResponseEntity(userTokenService.getAllTokensByUserID(userTokenDto),HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/countToken")
    public ResponseEntity<?> countUserToken
            (@RequestBody UserTokenDto userTokenDto)
    {
        if(userTokenService.tokenExistByUserId(userTokenDto))
        {
            return new ResponseEntity(
                    userTokenService.countUserToken(userTokenDto),HttpStatus.OK);
        }
        return new ResponseEntity("no token associated with this ID : '"+userTokenDto.getUserId()+"'"
                ,HttpStatus.NOT_FOUND);

    }


    //TODO nge PERSIST userToken pas kebuat AR/Contract Transaction, serta set type
}
