package com.hackathon.rocky.security;

import com.hackathon.rocky.dto.JWTAuthenticationToken;
import com.hackathon.rocky.dto.JWTUserDetails;
import com.hackathon.rocky.dto.JwtUser;
import com.hackathon.rocky.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class JWTAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    private JWTValidator validator;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {

    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {

        JWTAuthenticationToken jwtAuthenticationToken =(JWTAuthenticationToken) usernamePasswordAuthenticationToken;

        String token=jwtAuthenticationToken.getToken();

        JwtUser jwtUser=validator.validate(token);
        if (jwtUser == null) {
            throw new RuntimeException("SALAH TOKEN");
        }
        //kalau pake role
//        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
//                .commaSeparatedStringToAuthorityList(jwtUser.getRole)
        User user = new User();
        user.setUserName(jwtUser.getUserName());
        user.setUserPassword(jwtUser.getUserpassword());
        user.setUserId(jwtUser.getUserId());

        return new JWTUserDetails(user);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return (JWTAuthenticationToken.class.isAssignableFrom(aClass));
    }
}
