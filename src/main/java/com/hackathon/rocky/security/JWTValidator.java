package com.hackathon.rocky.security;

import com.hackathon.rocky.dto.JwtUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;

@Component
public class JWTValidator {
    private String secret="rocky";


    public JwtUser validate(String token) {

        JwtUser jwtUser = null;
        try {

        Claims body= Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token).getBody();

        jwtUser = new JwtUser();

        jwtUser.setUserName(body.getSubject());
        jwtUser.setUserId((String)body.get("userId"));
        jwtUser.setUserpassword((String)body.get("userpassword"));
        }
        catch (Exception e){
            System.out.println(e);
        }

        return jwtUser;
    }
}
