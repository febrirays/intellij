package com.hackathon.rocky.security;


import com.hackathon.rocky.dto.JwtUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

@Component
public class JWTGenerator {
    public String generate(JwtUser jwtUser) {

        Claims claims= Jwts.claims()
                .setSubject(jwtUser.getUserName());
        claims.put("userId",jwtUser.getUserId());
        claims.put("userName",jwtUser.getUserName());
        claims.put("userPoints",jwtUser.getUserPoint());


        return Jwts.builder().setClaims(claims)
                .signWith(SignatureAlgorithm.HS512,"rocky")
                .claim("userId",jwtUser.getUserId())
                .compact();

    }
}
