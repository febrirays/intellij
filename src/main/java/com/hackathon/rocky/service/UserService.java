package com.hackathon.rocky.service;

import com.hackathon.rocky.dto.JwtUser;
import com.hackathon.rocky.entity.User;

import java.io.IOException;
import java.util.List;


public interface UserService  {

    User saveUser(User user);
    List<User> findAll();

    String login(JwtUser jwtUser)throws IOException;

    User findUserbyId(String userId);
    User findByNameUser(String username);
}
