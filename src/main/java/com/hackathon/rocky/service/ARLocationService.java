package com.hackathon.rocky.service;

import com.hackathon.rocky.entity.ARLocation;
import org.springframework.web.multipart.MultipartFile;

public interface ARLocationService {
    void saveARLocation(ARLocation arLocation);
    void updateARLocation(ARLocation arLocation);
    void deleteARLocation(String arLocationId);
    ARLocation getLocationById(String arLocationId);
}
