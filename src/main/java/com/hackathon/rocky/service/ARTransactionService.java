package com.hackathon.rocky.service;

import com.hackathon.rocky.dto.ARTransactionDto;
import com.hackathon.rocky.dto.UserARHistoryDto;
import com.hackathon.rocky.entity.ARTransaction;

import java.util.List;

public interface ARTransactionService {
    void saveARTransaction(ARTransactionDto arTransactionDto);
    void updateARTransaction(ARTransaction arTransaction);
    void deleteARTransaction(String arTransactionId);
    ARTransaction checkArTransactionLocationIfAlreadyExists(String locationId);
    List<ARTransaction> getArTransactionByUserIdAndLocationId(String userId, String arLocationId);
    List<ARTransaction> findAll();
    List<UserARHistoryDto> getUserHistory(String userId);
    List<ARTransaction> findAllByUserId(String userId);

}
