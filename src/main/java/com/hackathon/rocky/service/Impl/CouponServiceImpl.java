package com.hackathon.rocky.service.Impl;

import com.hackathon.rocky.dao.CouponRepository;
import com.hackathon.rocky.dao.CouponTypeRepository;
import com.hackathon.rocky.dao.PartnerRepository;
import com.hackathon.rocky.dto.CouponDto;
import com.hackathon.rocky.entity.Coupon;
import com.hackathon.rocky.entity.CouponType;
import com.hackathon.rocky.entity.Partner;
import com.hackathon.rocky.entity.Transaction;
import com.hackathon.rocky.exceptionHandler.ErrorMessageException;
import com.hackathon.rocky.service.CouponService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
public class CouponServiceImpl implements CouponService {

    Logger log = LoggerFactory.getLogger(CouponServiceImpl.class);

    @Autowired
    private CouponRepository couponRepository;

    @Autowired
    private CouponTypeRepository couponTypeRepository;

    @Autowired
    private PartnerRepository partnerRepository;

    public CouponServiceImpl(CouponRepository CouponRepository){this.couponRepository = CouponRepository;}


//    public void getCoupon(Coupon coupon) {
////        couponRepository.findAll();
//    }




    public Coupon saveCoupon(CouponDto couponDto) {
        Partner partner = partnerRepository.getOne(couponDto.getPartnerId());
        CouponType couponType = couponTypeRepository.getOne(couponDto.getCouponTypeId());

        Coupon coupon = new Coupon();

        coupon.setCoupon_name(couponDto.getCouponName());
        coupon.setCoupon_desc(couponDto.getCouponDesc());
        coupon.setCoupon_price(couponDto.getCouponPrice());
        coupon.setCoupon_expire(couponDto.getCouponExp());




        coupon.setPartner(partner);
        coupon.setCouponType(couponType);

        couponRepository.save(coupon);
        return coupon;
    }



    @Override
    public void updateCoupon(Coupon coupon) {
        couponRepository.save(coupon);
    }

    @Override
    public void deleteCoupon(String coupon) {
        couponRepository.delete(coupon);
    }

    @Override
    public Coupon getCouponById(String couponId) {

        Coupon coupon = couponRepository.findOne(couponId);

        return coupon;
    }

    @Override
    public CouponDto getcouponDTO(String couponid) {
        CouponDto couponDto = new CouponDto();


        Coupon coupon = couponRepository.findOne(couponid);
        if(coupon == null){
            throw new ErrorMessageException(couponid + " is not found");
        }




        couponDto.setCouponId(coupon.getCoupon_id());
        couponDto.setCouponName(coupon.getCoupon_name());
        couponDto.setCouponDesc(coupon.getCoupon_desc());
        couponDto.setCouponPrice(coupon.getCoupon_price());
        InetAddress ip;
        try {

            ip = InetAddress.getLocalHost();

            couponDto.setCouponPic("http://" + ip.getHostAddress()+ ":8888/coupon/gambar?couponId=" + coupon.getCoupon_id());
            couponDto.setCouponPic("http://localhost:8888/coupon/gambar?couponId=" + coupon.getCoupon_id());


        } catch (UnknownHostException e) {

            e.printStackTrace();

        }





        return couponDto;
    }
    @Override
    public List<CouponDto> getAllCouponDto(String userId) {
        List<Coupon> coupons = couponRepository.findTransactionByUserIdBought(userId);
        List<CouponDto> couponDtos = new ArrayList<>();
//        Transaction transaction = new Transaction();
        for (Coupon coupon: coupons){
            CouponDto couponDto1 = new CouponDto();
            String status ="";

            couponDto1.setCouponId(coupon.getCoupon_id());
            couponDto1.setCouponName(coupon.getCoupon_name());
            couponDto1.setCouponPrice(coupon.getCoupon_price());
            couponDto1.setCouponDesc(coupon.getCoupon_desc());
            couponDto1.setCouponStatus("available");
//                InetAddress ip;
//                try {
//
//                    ip = InetAddress.getLocalHost();
//
//                    couponDto1.setCouponPic("http://" + ip.getHostAddress()+ ":8888/coupon/gambar?couponId=" + coupon.getCoupon_id());
//
//                } catch (UnknownHostException e) {
//
//                    e.printStackTrace();
//
//                }
            couponDto1.setCouponPic(coupon.getCoupon_gambar());
            couponDtos.add(couponDto1);
        }

        List<Coupon> coupons1 = couponRepository.findTransactionByUserIdNotBought(userId);

        for (Coupon coupon: coupons1){
            CouponDto couponDto1 = new CouponDto();

            String status ="";
            couponDto1.setCouponId(coupon.getCoupon_id());
            couponDto1.setCouponName(coupon.getCoupon_name());
            couponDto1.setCouponPrice(coupon.getCoupon_price());
            couponDto1.setCouponDesc(coupon.getCoupon_desc());
            couponDto1.setCouponStatus("bought");
//            InetAddress ip;
//            try {
//
//                ip = InetAddress.getLocalHost();
//
//                couponDto1.setCouponPic("http://" + ip.getHostAddress()+ ":8888/coupon/gambar?couponId=" + coupon.getCoupon_id());
//
//            } catch (UnknownHostException e) {
//
//                e.printStackTrace();
//
//            }
            couponDto1.setCouponPic(coupon.getCoupon_gambar());
            couponDtos.add(couponDto1);
        }



        return couponDtos;
    }




    @Override
    public void uploadImages(MultipartFile imageFile, String couponId) throws Exception {
        Coupon coupon = couponRepository.findOne(couponId);


       // String folder = "/assets/";
        //src/main/resource
//        Path currentPath = Paths.get(".");
//        Path absolutePath =  currentPath.toAbsolutePath();
        String address = "D:\\assets/";

            if(coupon.getCoupon_gambar() == null || coupon.getCoupon_gambar().isEmpty()) {

                String filename = imageFile.getOriginalFilename().replace(' ', '-');
//        coupon.setCoupon_gambar(address + "/src/main/resources/image/" + imageFile.getOriginalFilename());
                coupon.setCoupon_gambar(address + filename);

                byte[] bytes = imageFile.getBytes();
                Path path =  Paths.get(coupon.getCoupon_gambar());
                Files.write(path,bytes);


                couponRepository.save(coupon);
            }else{
                throw new ErrorMessageException("coupon with id: " + couponId + "already has a picture");
            }




    }

    @Override
    public void overwriteImages(MultipartFile imageFile, String couponId) throws Exception {

        Coupon coupon = couponRepository.findOne(couponId);
       Path filepath = Paths.get(coupon.getCoupon_gambar());
        try {
            Files.deleteIfExists(filepath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        coupon.setCoupon_gambar(null);
        String address = "D:\\assets/";

        String filename = imageFile.getOriginalFilename().replace(' ', '-');
        coupon.setCoupon_gambar(address + filename);
        byte[] bytes = imageFile.getBytes();
        Path path =  Paths.get(coupon.getCoupon_gambar());
        Files.write(path,bytes);


        couponRepository.save(coupon);

    }


    //buat nge POST image
//    @Override
//    public void saveImage(String couponId,MultipartFile file){
//        try {
//            Coupon coupon = couponRepository.findOne(couponId);
//
//            Byte[] byteObjects = new Byte[file.getBytes().length];
//
//            int i = 0;
//
//            for (byte b : file.getBytes()){
//                byteObjects[i++] = b;
//            }
//
//            coupon.setCoupon_picture(byteObjects);
//
//            couponRepository.save(coupon);
//        } catch (IOException e) {
//            //todo handle better
//            log.error("Error occurred", e);
//
//            e.printStackTrace();
//        }
//    }

    @Override
    public List<Coupon> getAllData() {

        return couponRepository.findAll();
    }


//    @Override
//    public void getCoupon(String coupon) {
//        couponRepository.getCouponByCoupon_id(coupon);
//    }

    String folder = "/photos/";
//        byte[] bytes = imageFile.getBytes();
//        Path path = Paths.get(folder + imageFile.getOriginalFilename());
//        Files.write(path,bytes);
}
