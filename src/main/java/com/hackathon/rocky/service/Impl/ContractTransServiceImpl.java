package com.hackathon.rocky.service.Impl;


import com.hackathon.rocky.dao.ContractTransactionRepository;
import com.hackathon.rocky.dao.UserRepository;
import com.hackathon.rocky.dao.UserTokenRepository;
import com.hackathon.rocky.dto.ContractTransactionDto;
import com.hackathon.rocky.entity.ContractTransaction;
import com.hackathon.rocky.entity.User;
import com.hackathon.rocky.entity.UserToken;
import com.hackathon.rocky.service.ContractTransService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContractTransServiceImpl implements ContractTransService {

    @Autowired
    private ContractTransactionRepository contractTransactionRepository;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserTokenRepository userTokenRepository;


    public ContractTransServiceImpl(ContractTransactionRepository contractTransactionRepository){this.contractTransactionRepository = contractTransactionRepository;}



    @Override
    public void savecontract(ContractTransactionDto contractTransactionDto){

        User user = userRepository.getOne(contractTransactionDto.getUser_id());

        ContractTransaction contractTransaction= new ContractTransaction();
        contractTransaction.setTenorPrice(contractTransactionDto.getTenor_price());
        contractTransaction.setTotalCicilan(contractTransactionDto.getTotal_cicilan());


        UserToken userToken= new UserToken();

        userToken.setUser(user);
        userToken.setTokenType("AR");
        userTokenRepository.save(userToken);

        contractTransaction.setUser(user);

        contractTransactionRepository.save(contractTransaction);
    }

    @Override
    public List<ContractTransaction> findAll(String userId) {
       return contractTransactionRepository.findTransactionByUserId(userId);
    }

    @Override
    public List<ContractTransactionDto> showhistory(String userId) {
        List<ContractTransactionDto> contractTransactionDtos = new ArrayList<>();
        List<ContractTransaction> contractTransactions= contractTransactionRepository.findTransactionByUserId(userId);

        for (ContractTransaction transaction : contractTransactions){
            ContractTransactionDto contractTransactionDto= new ContractTransactionDto();
            contractTransactionDto.setUser_id(transaction.getUser().getUserId());
            contractTransactionDto.setTenor_price(transaction.getTenorPrice());
            contractTransactionDto.setTotal_cicilan(transaction.getTotalCicilan());
            contractTransactionDtos.add(contractTransactionDto);
        }
        return contractTransactionDtos;

    }

}
