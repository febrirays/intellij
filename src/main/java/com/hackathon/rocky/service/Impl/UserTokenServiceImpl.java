package com.hackathon.rocky.service.Impl;

import com.hackathon.rocky.dao.UserRepository;
import com.hackathon.rocky.dao.UserTokenRepository;
import com.hackathon.rocky.dto.UserToken.UserTokenDto;
import com.hackathon.rocky.dto.UserToken.UserTokenResponseDto;
import com.hackathon.rocky.entity.User;
import com.hackathon.rocky.entity.UserToken;
import com.hackathon.rocky.service.UserTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.constraints.Null;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;


@Service
public class UserTokenServiceImpl implements UserTokenService {

    @Autowired
    private UserTokenRepository userTokenRepository;

    @Autowired
    private UserRepository userRepository;

    public UserTokenServiceImpl(UserTokenRepository userTokenRepository){this.userTokenRepository = userTokenRepository;}


    @Override
    public UserToken saveUserToken(UserTokenDto userTokenDto) {
        User user = userRepository.findOne(userTokenDto.getUserId());

        UserToken userToken = new UserToken(userTokenDto.getTokenType(), user,
                Timestamp.valueOf(LocalDateTime.now(ZoneId.systemDefault())));
        userTokenRepository.save(userToken);
        return userToken;
    }



    @Override
    public UserToken findOldestUserTokenByUserIDandtokenType(UserTokenDto userTokenDto) {
        String userID, tokenType;

        userID = userTokenDto.getUserId();
        tokenType = userTokenDto.getTokenType();

        return userTokenRepository.findTopByUserUserIdAndTokenTypeOrderByTokenDateTimeAsc
                (userID, tokenType);
    }

    @Override
    public String deleteSpecToken(UserTokenDto userTokenDto) {
        String chosenUT;
        UserToken chosenUserToken =
            userTokenRepository.findTopByUserUserIdAndTokenTypeOrderByTokenDateTimeAsc(
                    userTokenDto.getUserId(),
                    userTokenDto.getTokenType());
        chosenUT = chosenUserToken.getUserTokenId();
        userTokenRepository.delete(chosenUserToken);
        return "token associated with '"+
                userTokenDto.getUserId()+"' and '"+
                userTokenDto.getTokenType()+"' with UserTokenID :'"+
                chosenUT+"' deleted successfully";
    }

    @Override
    public String deleteAllTypeToken(UserTokenDto userTokenDto) {
        String chosenUT;
        UserToken chosenUserToken =
                userTokenRepository.findTopByUserUserIdOrderByTokenDateTimeAsc(
                            userTokenDto.getUserId());
        userTokenRepository.delete(chosenUserToken);
        chosenUT = chosenUserToken.getUserTokenId();
        return "token associated with '"+
                userTokenDto.getUserId()+"' with UserTokenID :'"+
                chosenUT+"' deleted successfully";
    }

    @Override
    public UserTokenResponseDto countUserToken(UserTokenDto userTokenDto) {
        String userID;

        userID = userTokenDto.getUserId();

        UserTokenResponseDto userTokenResponseDto = new UserTokenResponseDto();

        userTokenResponseDto.setTokenAR(
                userTokenRepository.countUserTokensByUserUserIdAndTokenType(userID, "AR"));
        userTokenResponseDto.setTokenTR(
                userTokenRepository.countUserTokensByUserUserIdAndTokenType(userID,"TR"));
        userTokenResponseDto.setTotalToken(
                userTokenResponseDto.getTokenAR()+
                        userTokenResponseDto.getTokenTR());

        userTokenResponseDto.setUserID(userID);
        userTokenResponseDto.setMessage("-");

        return userTokenResponseDto;
    }

    @Override
    public boolean userExistsByUserId(UserTokenDto userTokenDto) {
        return userRepository.exists(userTokenDto.getUserId());
    }

    @Override
    public boolean tokenExistByUserIdAndTokenType(UserTokenDto userTokenDto) {
        String userID, tokenType;
        userID = userTokenDto.getUserId();
        tokenType = userTokenDto.getTokenType();
        return userTokenRepository.existsByUserUserIdAndTokenType(userID,tokenType);
    }

    @Override
    public boolean tokenExistByUserId(UserTokenDto userTokenDto) {
        return userTokenRepository.existsByUserUserId(userTokenDto.getUserId());
    }

    @Override
    public List getAllTokens(UserTokenDto userTokenDto) {
        return userTokenRepository.findAllByOrderByUser();
    }

    @Override
    public List getAllTokensByUserID(UserTokenDto userTokenDto) {
        return userTokenRepository.findAllByUserUserIdOrderByTokenDateTime(
                userTokenDto.getUserId());
    }

    @Override
    public void deleteByUTID(String userTokenID) {
        userTokenRepository.delete(userTokenID);
    }

}
