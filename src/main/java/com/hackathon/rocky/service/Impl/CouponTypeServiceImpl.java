package com.hackathon.rocky.service.Impl;


import com.hackathon.rocky.dao.CouponTypeRepository;

import com.hackathon.rocky.entity.CouponType;
import com.hackathon.rocky.service.CouponTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CouponTypeServiceImpl implements CouponTypeService {

    @Autowired
    private CouponTypeRepository couponTypeRepository;

    public CouponTypeServiceImpl(CouponTypeRepository couponTypeRepository){this.couponTypeRepository = couponTypeRepository;}

    public void saveCouponType(CouponType couponType){

        couponTypeRepository.save(couponType);
    }

    public void UpdateCouponType(CouponType couponType){couponTypeRepository.save(couponType);}

    public void DeleteCouponType(String couponType){couponTypeRepository.delete(couponType);}
//
//    @Override
//    public void find(String partner) {
//        couponTypeRepository.findallB;
//    }


}
