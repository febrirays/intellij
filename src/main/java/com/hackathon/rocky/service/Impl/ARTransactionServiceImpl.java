package com.hackathon.rocky.service.Impl;

import com.hackathon.rocky.dao.ARLocationRepository;
import com.hackathon.rocky.dao.ARTransactionRepository;
import com.hackathon.rocky.dao.UserRepository;
import com.hackathon.rocky.dao.UserTokenRepository;
import com.hackathon.rocky.dto.ARTransactionDto;
import com.hackathon.rocky.dto.UserARHistoryDto;
import com.hackathon.rocky.entity.ARLocation;
import com.hackathon.rocky.entity.ARTransaction;
import com.hackathon.rocky.entity.User;
import com.hackathon.rocky.entity.UserToken;
import com.hackathon.rocky.service.ARTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ARTransactionServiceImpl implements ARTransactionService {

    @Autowired
    private ARTransactionRepository arTransactionRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ARLocationRepository arLocationRepository;

    @Autowired
    private UserTokenRepository userTokenRepository;

    @Override
    public void saveARTransaction(ARTransactionDto arTransactionDto) {
        User user = userRepository.getOne(arTransactionDto.getUserId());
        ARLocation arLocation = arLocationRepository.getOne(arTransactionDto.getArLocationId());

        ARTransaction arTransaction = new ARTransaction();
        arTransaction.setUser(user);
        arTransaction.setArLocation(arLocation);

        UserToken userToken = new UserToken();

        userToken.setUser(user);
        userToken.setTokenType("AR");
        userTokenRepository.save(userToken);

        arTransactionRepository.save(arTransaction);
    }

    @Override
    public void updateARTransaction(ARTransaction arTransaction) {
        arTransactionRepository.save(arTransaction);
    }

    @Override
    public void deleteARTransaction(String arTransactionId) {
        arTransactionRepository.delete(arTransactionId);
    }

    @Override
    public ARTransaction checkArTransactionLocationIfAlreadyExists(String locationId) {
        return arTransactionRepository.checkArTransactionLocationIfAlreadyExists(locationId);
    }

    @Override
    public List<ARTransaction> getArTransactionByUserIdAndLocationId(String userId, String arLocationId) {
        return arTransactionRepository.getArTransactionByUserIdAndLocationId(userId, arLocationId);
    }

    @Override
    public List<ARTransaction> findAll() {
        return arTransactionRepository.findAll();
    }

    @Override
    public List<UserARHistoryDto> getUserHistory(String userId) {
        List<UserARHistoryDto> userARHistoryDtos = new ArrayList<>();
        List<ARTransaction> arTransactions = arTransactionRepository.findTransactionByUserId(userId);

        for(ARTransaction transaction : arTransactions){
            UserARHistoryDto userARHistoryDto = new UserARHistoryDto();

            userARHistoryDto.setArTransactionId(transaction.getArTransactionId());
            userARHistoryDto.setUserName(transaction.getUser().getUserName());
            userARHistoryDto.setUserPoint(transaction.getUser().getUserPoint());
            userARHistoryDto.setLocationName(transaction.getArLocation().getLocationName());
            userARHistoryDto.setArTransactionDay(transaction.getArTransactionTime().getDayOfWeek().name());
            userARHistoryDto.setArTransactionDate(transaction.getArTransactionTime().getDayOfMonth());
            userARHistoryDto.setArTransactionMonth(transaction.getArTransactionTime().getMonthValue());
            userARHistoryDto.setArTransactionYear(transaction.getArTransactionTime().getYear());

            userARHistoryDtos.add(userARHistoryDto);
        }
        return userARHistoryDtos;
    }

    @Override
    public List<ARTransaction> findAllByUserId(String userId) {
        return arTransactionRepository.findTransactionByUserId(userId);
    }
}
