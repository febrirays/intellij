package com.hackathon.rocky.service.Impl;


import com.hackathon.rocky.dao.PartnerRepository;
import com.hackathon.rocky.entity.Partner;
import com.hackathon.rocky.service.PartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PartnerServiceImpl implements PartnerService {

    @Autowired
    private PartnerRepository partnerRepository;

    public PartnerServiceImpl(PartnerRepository PartnerRepository){this.partnerRepository = PartnerRepository;}


    public void savePartner(Partner partner){partnerRepository.save(partner);}

    @Override
    public void updatePartner(Partner partner) { partnerRepository.save(partner); }

    @Override
    public void deletePartner(String partner) {
        partnerRepository.delete(partner);
    }

//    @Override
//    public void find(String partner) {
//        partnerRepository.findAllByPartner_address(partner);
//    }


}
