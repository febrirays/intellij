package com.hackathon.rocky.service.Impl;

import com.hackathon.rocky.dao.UserRepository;
import com.hackathon.rocky.dto.ErrorExc;
import com.hackathon.rocky.dto.JWTAuthenticationToken;
import com.hackathon.rocky.dto.JwtUser;
import com.hackathon.rocky.dto.UserDto;
import com.hackathon.rocky.entity.User;
import com.hackathon.rocky.security.JWTGenerator;
import com.hackathon.rocky.service.UserService;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    private UserDto userDto;

    @Autowired
    CustomUserDetailsService customUserDetailsService;
    @Autowired
    private JWTGenerator jwtGenerator;

    public UserServiceImpl(UserRepository userRepository,PasswordEncoder passwordEncoder){
        this.userRepository = userRepository;
    this.passwordEncoder=passwordEncoder;
    }

    PasswordEncoder passwordEncoder;

    //ini update
//    @Override
//    public User saveUser(UserDto userDto){
//        User user = userRepository.getOne(userDto.User_id);
//        user.setUser_name(userDto.getUser_name());
////        userDto.setUser_id();
////        User user = new User();
//        userRepository.save(user);
//        return user;
//    }

    @Override
    public User saveUser(User user){
        user.setUserPoint(0);
            user.setUserPassword(passwordEncoder.encode(user.getUserPassword()));

        return userRepository.save(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public String login(JwtUser jwtUser)throws IOException{
        customUserDetailsService.loadUserByUsername(jwtUser.getUserName());
        User user = userRepository.findByUserName(jwtUser.getUserName());

        String password= jwtUser.getUserpassword();
        if(!passwordEncoder.matches(password,user.getUserPassword())){

            throw new ErrorExc();
        }
        else {

            jwtUser.setUserId(user.getUserId());
            JWTAuthenticationToken jwtAuthenticationToken = new JWTAuthenticationToken(jwtGenerator.generate(jwtUser));
            jwtUser.setUserPoint(user.getUserPoint());
            jwtAuthenticationToken.setToken(jwtGenerator.generate(jwtUser));

            String payload = jwtAuthenticationToken.getToken().split("\\.")[1];
            return new String(Base64.decodeBase64(payload), "UTF-8");

        }

    }


    @Override
    public User findUserbyId(String userId) {
       return userRepository.getOne(userId);
    }

    @Override
    public User findByNameUser(String username) {
        return userRepository.findByUserName(username);
    }


}
