package com.hackathon.rocky.service.Impl;


import com.hackathon.rocky.Sequence.generatorId;
import com.hackathon.rocky.dao.CouponRepository;
import com.hackathon.rocky.dao.CouponTransactionRepository;
import com.hackathon.rocky.dao.UserRepository;
import com.hackathon.rocky.dto.CouponRequest;
import com.hackathon.rocky.dto.ResponseDTO;
import com.hackathon.rocky.dto.UserHistoryDto;
import com.hackathon.rocky.entity.*;
import com.hackathon.rocky.service.CouponTransactionService;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;
import java.text.ParseException;
import java.util.stream.Collectors;

@Service
public class CouponTransactionImpl implements CouponTransactionService {





    @Autowired
    private CouponTransactionRepository couponTransactionRepository;

    @Autowired
    private CouponRepository couponRepository;

    @Autowired
    private UserRepository userRepository;

    public CouponTransactionImpl(CouponTransactionRepository couponTransactionRepository) {
        this.couponTransactionRepository = couponTransactionRepository;
    }


    private UserHistoryDto collect(Transaction trx) {
        UserHistoryDto dto = new UserHistoryDto();
        dto.setTransactionId(trx.getTrans_id());
        dto.setDate(trx.getTrans_time().getDayOfMonth());
        dto.setDay(trx.getTrans_time().getDayOfWeek().name());
//        dto.setDay(trx.getUser().getUserId());
        dto.setMonth(trx.getTrans_time().getMonth().name());
        dto.setPointAffected(trx.getPoints_affected());
        dto.setTransType(trx.getTrans_Type());
        if(trx.getTrans_Type().equals("Coupon")) {
            dto.setTitleName(trx.getCoupon().getCoupon_name());
        }else if(trx.getTrans_Type().equals("Point")) {
            dto.setTitleName("Token Exchange");
        }
        return dto;
    }


//    @Override
//    public int compare(String s1, String s2) {
//        return s1.length() == s2.length() ? s1.compareTo(s2) : s1.length() - s2.length();
//    }


    //get all transaction according to user ID
    //TODO tambahin maps + year
    @Override
    public ResponseDTO findAllTransactionByUserID(String userId){
        ResponseDTO responseDTO = new ResponseDTO();

        Map<String,Integer> dayValues = new HashMap<>();
        dayValues.put("JANUARY",1);
        dayValues.put("FEBRUARY",2);
        dayValues.put("MARCH",3);
        dayValues.put("APRIL",4);
        dayValues.put("MAY",5);
        dayValues.put("JUNE",6);
        dayValues.put("JULY",7);
        dayValues.put("AUGUST",8);
        dayValues.put("SEPTEMBER",9);
        dayValues.put("OCTOBER",10);
        dayValues.put("NOVEMBER",11);
        dayValues.put("DECEMBER",12);
        Comparator<String> cmpDays = Comparator.comparing(dayValues::get,Comparator.reverseOrder());

        Map<String, Collection<UserHistoryDto>> maps = new TreeMap<>(cmpDays);
        List<Transaction> transactions = couponTransactionRepository.findTransactionByUserId(userId);
        User user = userRepository.findOne(userId); //set the object based on ID

        if(user != null) {

            Integer date;
            String month = "";
            responseDTO.setUserPoint(user.getUserPoint()); //Get user-point

            for (Transaction trx : transactions) {

                //get the value of days as a map key
                month = trx.getTrans_time().getMonth().name();
                Collection<UserHistoryDto> col = maps.getOrDefault(month, new ArrayList<>());
                col.add(collect(trx));
                maps.put(month, col);
            }


            responseDTO.setHistories(maps);
        }else{
            throw  new EntityNotFoundException("the id: '" + userId + "' can't be found,");
        }

        //DateFormatte
        //TODO buat sehingga get.userpoint gak looping\

//        for(Transaction transaction : transactions){
//            responseDTO.setUserPoint(transaction.getUser().getUser_point()); //Get (looping) user-point
//            UserHistoryDto userHistoryDto = new UserHistoryDto();
//
//            //get all needed data to show to front end
//            userHistoryDto.setTransactionId(transaction.getTrans_id());
//            userHistoryDto.setPointAffected(transaction.getPoints_affected());
//
//            //Todo urutin berasarkan bulan , baru date
//            //get dates, day, month
//            userHistoryDto.setDate(transaction.getTrans_time().getDayOfMonth());
//            userHistoryDto.setDay(transaction.getTrans_time().getDayOfWeek().name());
//            userHistoryDto.setMonth(transaction.getTrans_time().getMonth().name());
//
//
//            String type = transaction.getTrans_Type();
//            userHistoryDto.setTransType(type);
//            if(type.matches("Coupon")) {
//                userHistoryDto.setCouponDesc(transaction.getCoupon().getCoupon_desc());
//                userHistoryDto.setTitleName(transaction.getCoupon().getCoupon_name());
//            }else{
//                userHistoryDto.setTitleName("Token Exchange");
//            }
//
//
//            //userHistoryDto.setCouponPic(transaction.getCoupon().getCoupon_picture());
//
//            userHistoryDtos.add(userHistoryDto);
//        }
//        responseDTO.setUserHistories(userHistoryDtos);

        return responseDTO;
    }

    @Override
    public Transaction getOneTransaction(String transId) {

        //UserHistoryDto userHistoryDto = new UserHistoryDto();
        Transaction transaction = couponTransactionRepository.getOne(transId);

        //userHistoryDto.setDate();

        return transaction;
    }

    //TODO Front end nge kirim data ke backend apa aja??

    //Input transaksi baru, sekaligus konekin antar user dan kupon
    public Transaction saveCouponTrans(CouponRequest couponRequest) {


        //String couponId =couponRequest.getCouponId();
        Coupon coupon = couponRepository.getOne(couponRequest.getCouponId());
        User user = userRepository.getOne(couponRequest.getUserId());

        //nge set current point dan point user setelah transaksi
        Transaction transaction = new Transaction();
        //transaction.setCurrent_point(user.getUser_point() - coupon.getCoupon_price());
        //user.setUser_point(user.getUser_point() - coupon.getCoupon_price());


            //beli KUPON
            transaction.setPoints_affected(coupon.getCoupon_price());
            transaction.setCurrent_point(user.getUserPoint() - coupon.getCoupon_price());
            user.setUserPoint(user.getUserPoint() - coupon.getCoupon_price());
            transaction.setTrans_Type("Coupon");

    //            //manually change date
    //            LocalDateTime months = transaction.getTrans_time();
    //            LocalDateTime changemonths = months.plusMonths(3);
    //            transaction.setTrans_time(changemonths);



        transaction.setCoupon(coupon); //re learn this
        transaction.setUser(user);



        //buat jadi ngurangin currentpoint

        couponTransactionRepository.save(transaction);
        return transaction;
    }
    //setelah dapet baru jalanin yang save couponTrans

    @Override
    public Transaction savePoint(CouponRequest pointTransaction) {

        //param : userId & pointAffected
        User user = userRepository.getOne(pointTransaction.getUserId());

        //nge set current point dan point user setelah transaksi
        Transaction transaction = new Transaction();

        //TODO auto generate ID jadi point ID
        //TODO buat function sequence
        //TODO ganti parameter point yang diubah saat terima transaksi baru

        //Integer time = LocalDateTime.now().getHour() + LocalDateTime.now().getMinute() + LocalDateTime.now().getSecond();

        transaction.setTrans_id("PON" + (LocalDateTime.now().toString()));

        //dapet POINT
        //Terima feedbackd dari front end
        transaction.setPoints_affected(pointTransaction.getPointAffected()); //dummy
        transaction.setCurrent_point(user.getUserPoint() + pointTransaction.getPointAffected()); //dummy
        user.setUserPoint(transaction.getCurrent_point());
        transaction.setTrans_Type("Point");


                    //manually change date
                    LocalDateTime months = transaction.getTrans_time();
                    LocalDateTime changemonths = months.plusMonths(2);
                    transaction.setTrans_time(changemonths);


        transaction.setUser(user);

        couponTransactionRepository.save(transaction);

        return transaction;
    }


    public void updateCouponTrans(Transaction transaction) {
        couponTransactionRepository.save(transaction);
    }

    public void deleteCouponTrans(String couponTransaction) {
        couponTransactionRepository.delete(couponTransaction);
    }


    @Override
    public  List<Transaction> getAllData() {
       return couponTransactionRepository.findAll();
    }





}