package com.hackathon.rocky.service;

import com.hackathon.rocky.entity.Partner;

public interface PartnerService {

    void savePartner (Partner partner);
    void updatePartner (Partner partner);
    void deletePartner (String partner);
//    void find(String partner);
}
