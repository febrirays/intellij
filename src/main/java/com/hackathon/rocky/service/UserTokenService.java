package com.hackathon.rocky.service;

import com.hackathon.rocky.dto.UserToken.UserTokenDto;
import com.hackathon.rocky.dto.UserToken.UserTokenResponseDto;
import com.hackathon.rocky.entity.UserToken;

import java.util.List;

public interface UserTokenService {

    UserToken saveUserToken(UserTokenDto userTokenDto);
    UserToken findOldestUserTokenByUserIDandtokenType(UserTokenDto userTokenDto);

    String deleteSpecToken(UserTokenDto userTokenDto);
    String deleteAllTypeToken(UserTokenDto userTokenDto);


    UserTokenResponseDto countUserToken(UserTokenDto userTokenDto);

    boolean userExistsByUserId(UserTokenDto userTokenDto);
    boolean tokenExistByUserIdAndTokenType(UserTokenDto userTokenDto);
    boolean tokenExistByUserId(UserTokenDto userTokenDto);

    List getAllTokens(UserTokenDto userTokenDto);
    List getAllTokensByUserID(UserTokenDto userTokenDto);


    void deleteByUTID(String userTokenID);


}
