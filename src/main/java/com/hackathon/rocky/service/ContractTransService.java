package com.hackathon.rocky.service;

import com.hackathon.rocky.dto.ContractTransactionDto;
import com.hackathon.rocky.entity.ContractTransaction;
import java.util.List;

public interface ContractTransService {
    void savecontract(ContractTransactionDto contractTransactionDto);
    List<ContractTransaction> findAll(String userId);
    List<ContractTransactionDto> showhistory(String userId);
}
