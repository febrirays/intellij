package com.hackathon.rocky.service;

import com.hackathon.rocky.dto.CouponRequest;
import com.hackathon.rocky.dto.ResponseDTO;
import com.hackathon.rocky.dto.UserHistoryDto;
import com.hackathon.rocky.entity.Transaction;

import java.text.ParseException;
import java.util.List;

public interface CouponTransactionService {
    ResponseDTO findAllTransactionByUserID(String userId);
//  Transaction getByAllTransactionByUserID(CouponTransactionDto couponTransactionDto);

    Transaction getOneTransaction(String transId);
    Transaction saveCouponTrans(CouponRequest couponTransaction);
    Transaction savePoint(CouponRequest couponTransaction);

    void updateCouponTrans(Transaction transaction);
    void deleteCouponTrans (String couponTransaction);
    List<Transaction> getAllData();
}