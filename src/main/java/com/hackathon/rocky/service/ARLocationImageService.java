package com.hackathon.rocky.service;

import org.springframework.web.multipart.MultipartFile;

public interface ARLocationImageService {
    void saveARLocationImage(String arLocationId, MultipartFile multipartFile);
}
