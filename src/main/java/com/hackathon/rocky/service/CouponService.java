package com.hackathon.rocky.service;

import com.hackathon.rocky.dto.CouponDto;
import com.hackathon.rocky.entity.Coupon;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;



public interface CouponService {

    Coupon saveCoupon(CouponDto couponDto);
    void updateCoupon(Coupon coupon);
    void deleteCoupon(String coupon);
    Coupon getCouponById(String couponid);
    CouponDto getcouponDTO(String couponid);

    void uploadImages(MultipartFile imageFile, String couponId) throws Exception;
    void overwriteImages(MultipartFile imageFile, String couponId) throws Exception;

   // void saveImage(String couponId, MultipartFile file);
    List<Coupon> getAllData();

    List<CouponDto> getAllCouponDto(String userId);

    //void uploadImage(MultipartFile imageFile) throws Exception ;
//    void getCoupon(String coupon);



}
