package com.hackathon.rocky.dto;

import java.util.Objects;

public class RegisterDto {
    public String userName;
    public String userPassword;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegisterDto that = (RegisterDto) o;
        return userPassword.equals(that.userPassword);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userPassword);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
}
