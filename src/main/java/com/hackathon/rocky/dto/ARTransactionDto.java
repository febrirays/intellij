package com.hackathon.rocky.dto;

public class ARTransactionDto {
    public String arLocationId;
    public String userId;

    public String getArLocationId() {
        return arLocationId;
    }

    public void setArLocationId(String arLocationId) {
        this.arLocationId = arLocationId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
