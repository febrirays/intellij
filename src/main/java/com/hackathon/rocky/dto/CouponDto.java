package com.hackathon.rocky.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@NoArgsConstructor
public class CouponDto {

    private String couponId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String couponTypeId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String partnerId;

    @NotEmpty(message = "please provide name... DUH")
    private String couponName;

    @NotNull(message = "price cant be empty")
    private Integer couponPrice;

    @NotEmpty(message = "cant be empty")
    private String couponDesc;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String couponExp;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String couponPic;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String couponStatus;

    //setter and getter


}
