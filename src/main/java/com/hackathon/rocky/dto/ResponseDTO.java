package com.hackathon.rocky.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.List;
import java.util.Map;


@Getter
@Setter
@NoArgsConstructor
public class ResponseDTO {
    private int userPoint;
    //private List<UserHistoryDto> userHistories;
    private Map<String, Collection<UserHistoryDto>> histories;

    public int getUserPoint() {
        return userPoint;
    }

    public void setUserPoint(int userPoint) {
        this.userPoint = userPoint;
    }

    public Map<String, Collection<UserHistoryDto>> getHistories() {
        return histories;
    }

    public void setHistories(Map<String, Collection<UserHistoryDto>> histories) {
        this.histories = histories;
    }
}
