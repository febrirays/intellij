package com.hackathon.rocky.dto;

public class UserDto {
    public String User_name;
//    public String User_id;
    public int user_point;

    public int getUser_point() {
        return user_point;
    }

    public void setUser_point(int user_point) {
        this.user_point = user_point;
    }

    public String getUser_name() {
        return User_name;
    }

    public void setUser_name(String user_name) {
        User_name = user_name;
    }
//
//    public String getUser_id() {
//        return User_id;
//    }
//
//    public void setUser_id(String user_id) {
//        User_id = user_id;
//    }
}
