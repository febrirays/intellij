package com.hackathon.rocky.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.time.LocalDateTime;


@Data
public class UserHistoryDto {
    private String transactionId;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    private String userName;
    private Integer date;
    private String day;
    private String Month;
    //private String

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String titleName;
    private Integer pointAffected;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String couponDesc;
    private String transType;


    public String getCouponDesc() {
        return couponDesc;
    }

    public void setCouponDesc(String couponDesc) {
        this.couponDesc = couponDesc;
    }

    private Byte[] couponPic;


    @JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
    public Byte[] getCouponPic() {
        return couponPic;
    }

    public void setCouponPic(Byte[] couponPic) {
        this.couponPic = couponPic;
    }

    @JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
    public String getUserName() {
        return userName;
    }


    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return Month;
    }

    public void setMonth(String month) {
        Month = month;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    public Integer getPointAffected() {
        return pointAffected;
    }

    public void setPointAffected(Integer pointAffected) {
        this.pointAffected = pointAffected;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }
}
