package com.hackathon.rocky.dto;

import java.time.LocalDateTime;

public class UserARHistoryDto {
    public String arTransactionId;
    public String userName;
    public int userPoint;
    public String arTransactionDay;
    public int arTransactionDate, arTransactionMonth, arTransactionYear;
    public String locationName;

    public String getArTransactionDay() {
        return arTransactionDay;
    }

    public void setArTransactionDay(String arTransactionDay) {
        this.arTransactionDay = arTransactionDay;
    }

    public int getArTransactionDate() {
        return arTransactionDate;
    }

    public void setArTransactionDate(int arTransactionDate) {
        this.arTransactionDate = arTransactionDate;
    }

    public int getArTransactionMonth() {
        return arTransactionMonth;
    }

    public void setArTransactionMonth(int arTransactionMonth) {
        this.arTransactionMonth = arTransactionMonth;
    }

    public int getArTransactionYear() {
        return arTransactionYear;
    }

    public void setArTransactionYear(int arTransactionYear) {
        this.arTransactionYear = arTransactionYear;
    }

    public String getArTransactionId() {
        return arTransactionId;
    }

    public void setArTransactionId(String arTransactionId) {
        this.arTransactionId = arTransactionId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserPoint() {
        return userPoint;
    }

    public void setUserPoint(int userPoint) {
        this.userPoint = userPoint;
    }
}
