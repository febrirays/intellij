package com.hackathon.rocky.dto;

import java.math.BigInteger;

public class ContractTransactionDto {

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String user_id;
    public BigInteger total_cicilan;
    public BigInteger tenor_price;

    public BigInteger getTotal_cicilan() {
        return total_cicilan;
    }

    public void setTotal_cicilan(BigInteger total_cicilan) {
        this.total_cicilan = total_cicilan;
    }

    public BigInteger getTenor_price() {
        return tenor_price;
    }

    public void setTenor_price(BigInteger tenor_price) {
        this.tenor_price = tenor_price;
    }

}
