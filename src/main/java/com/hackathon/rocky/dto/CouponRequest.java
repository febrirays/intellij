package com.hackathon.rocky.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;


@Getter
@Setter
@NoArgsConstructor
public class CouponRequest {
    private String userId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String couponId;

    private LocalDateTime dates;

    private Integer currentPoint;
    private String transType;
    private Integer pointAffected;


}
