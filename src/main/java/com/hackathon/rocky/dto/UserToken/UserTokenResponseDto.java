package com.hackathon.rocky.dto.UserToken;

public class UserTokenResponseDto {
    private String userID;
    private String tokenType;
    private Integer tokenAR;
    private Integer tokenTR;
    private Integer totalToken;
    private String message;



    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public Integer getTokenAR() {
        return tokenAR;
    }

    public void setTokenAR(Integer tokenAR) {
        this.tokenAR = tokenAR;
    }

    public Integer getTokenTR() {
        return tokenTR;
    }

    public void setTokenTR(Integer tokenTR) {
        this.tokenTR = tokenTR;
    }

    public Integer getTotalToken() {
        return totalToken;
    }

    public void setTotalToken(Integer totalToken) {
        this.totalToken = totalToken;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
