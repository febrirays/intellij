package com.hackathon.rocky.dto.UserToken;


import lombok.Data;

//@Data
public class UserTokenDto {

    private String userId;

    private String tokenType;

    //private String tokenPoint;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }



}
