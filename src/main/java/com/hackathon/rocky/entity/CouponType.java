package com.hackathon.rocky.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "Coupon_Type")
@DynamicUpdate

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class CouponType  {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name="Coupon_type_id")
    private String Coupon_type_id;

    @NotEmpty(message = "coupon name cant be empty")
    @Column(name="Coupon_type_name")
    private String Coupon_type_name;

//    @OneToMany(mappedBy = "CouponType")
//    private List<Coupon> Coupon;

    public String getCoupon_type_id() {
        return Coupon_type_id;
    }

    public void setCoupon_type_id(String coupon_type_id) {
        Coupon_type_id = coupon_type_id;
    }

    public String getCoupon_type_name() {
        return Coupon_type_name;
    }

    public void setCoupon_type_name(String coupon_type_name) {
        Coupon_type_name = coupon_type_name;
    }

//    public List<com.hackathon.rocky.entity.Coupon> getCoupon() {
//        return Coupon;
//    }
//
//    public void setCoupon(List<com.hackathon.rocky.entity.Coupon> coupon) {
//        Coupon = coupon;
//    }
}
