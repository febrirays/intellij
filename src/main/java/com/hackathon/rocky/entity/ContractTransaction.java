package com.hackathon.rocky.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.hackathon.rocky.generator.StringSequenceIdentifier;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigInteger;

@Data
@Entity
@Table(name = "Contract_Transaction")
@DynamicUpdate
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class ContractTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "contract_seq")
    @GenericGenerator(name="contract_seq",strategy = "com.hackathon.rocky.generator.StringSequenceIdentifier",parameters = {
            @org.hibernate.annotations.Parameter(name = StringSequenceIdentifier.INCREMENT_PARAM, value = "1"),
            @org.hibernate.annotations.Parameter(name = StringSequenceIdentifier.VALUE_PREFIX_PARAMETER, value = "CONTRACT"),
            @org.hibernate.annotations.Parameter(name = StringSequenceIdentifier.NUMBER_FORMAT_PARAMETER, value = "%05d")
    })
    @Column(name = "Contract_Transaction_id")
    private String contractTransactionId;

    @Column(name = "Total_Cicilan")
    private BigInteger totalCicilan;

    @Column(name = "Tenor_Price")
    private BigInteger tenorPrice;


    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "User_id")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getContractTransactionId() {
        return contractTransactionId;
    }

    public void setContractTransactionId(String contractTransactionId) {
        this.contractTransactionId = contractTransactionId;
    }

    public BigInteger getTotalCicilan() {
        return totalCicilan;
    }

    public void setTotalCicilan(BigInteger totalCicilan) {
        this.totalCicilan = totalCicilan;
    }

    public BigInteger getTenorPrice() {
        return tenorPrice;
    }

    public void setTenorPrice(BigInteger tenorPrice) {
        this.tenorPrice = tenorPrice;
    }
}
