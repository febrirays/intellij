package com.hackathon.rocky.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.hackathon.rocky.Sequence.generatorId;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.persister.walking.internal.FetchStrategyHelper;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;


@Data
@Entity
@Table(name = "Coupon")
@DynamicUpdate
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class Coupon implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "book_seq")
    @GenericGenerator(
            name = "book_seq",
            strategy = "com.hackathon.rocky.Sequence.generatorId" +
                    "",
            parameters = {
                    @Parameter(name = generatorId.INCREMENT_PARAM, value = "50"),
                    @Parameter(name = generatorId.VALUE_PREFIX_PARAMETER, value = "CPN_"),
                    @Parameter(name = generatorId.NUMBER_FORMAT_PARAMETER, value = "%05d")})

    @Column(name = "Coupon_id")
    private String Coupon_id;

    @NotEmpty(message = "Please provide a name")
    @Column(name = "Coupon_name")
    private String Coupon_name;

    @NotNull(message = "Please provide price")
    @DecimalMin("1.00")
    @Column(name = "Coupon_price")
    private Integer Coupon_price;


    @Column
    private String Coupon_gambar;
     @NotEmpty(message = "Please provide description")
    @Column(name = "Coupon_desc")
    private String Coupon_desc;


    @Column(name = "Coupon_created")
    private LocalDateTime Coupon_created = LocalDateTime.now();

    @Column(name = "Coupon_expire")
    private String Coupon_expire;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "Coupon_type_id")
    private CouponType CouponType;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "Partner_id")
    private Partner Partner;

    public String getCoupon_id() {
        return Coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        Coupon_id = coupon_id;
    }

    public String getCoupon_name() {
        return Coupon_name;
    }

    public void setCoupon_name(String coupon_name) {
        Coupon_name = coupon_name;
    }

    public Integer getCoupon_price() {
        return Coupon_price;
    }

    public void setCoupon_price(Integer coupon_price) {
        Coupon_price = coupon_price;
    }

    public String getCoupon_desc() {
        return Coupon_desc;
    }

    public void setCoupon_desc(String coupon_desc) {
        Coupon_desc = coupon_desc;
    }

    public LocalDateTime getCoupon_created() {
        return Coupon_created;
    }

    public void setCoupon_created(LocalDateTime coupon_created) {
        Coupon_created = coupon_created;
    }

    public String getCoupon_expire() {
        return Coupon_expire;
    }

    public void setCoupon_expire(String coupon_expire) {
        Coupon_expire = coupon_expire;
    }

    public com.hackathon.rocky.entity.CouponType getCouponType() {
        return CouponType;
    }

    public void setCouponType(com.hackathon.rocky.entity.CouponType couponType) {
        CouponType = couponType;
    }

    public com.hackathon.rocky.entity.Partner getPartner() {
        return Partner;
    }

    public void setPartner(com.hackathon.rocky.entity.Partner partner) {
        Partner = partner;
    }
}
    //@OneToMany(mappedBy = "Transaction", cascade = CascadeType.ALL)

