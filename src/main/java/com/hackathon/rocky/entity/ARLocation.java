package com.hackathon.rocky.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import com.hackathon.rocky.generator.StringSequenceIdentifier;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "AR_Location")
@DynamicUpdate
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})

public class ARLocation{
    @Id
    @GeneratedValue(generator = "location-sequence", strategy = GenerationType.SEQUENCE)
    @GenericGenerator(
            name = "location-sequence",
            strategy = "com.hackathon.rocky.generator.StringSequenceIdentifier",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = StringSequenceIdentifier.INCREMENT_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = StringSequenceIdentifier.VALUE_PREFIX_PARAMETER, value = "AL_"),
                    @org.hibernate.annotations.Parameter(name = StringSequenceIdentifier.NUMBER_FORMAT_PARAMETER, value = "%05d")
            }
    )
    private String locationId;

    @Column(name = "LOCATION_NAME")
    private String locationName;

    @Column(name = "LOCATION_ADDRESS")
    private String locationAddress;

    @NotNull(message = "Longitude cannot be null.")
    @Column(name = "LONGITUDE")
    private Double longitude;

    @NotNull(message = "Latitude cannot be null.")
    @Column(name = "LATITUDE")
    private Double latitude;

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
}
