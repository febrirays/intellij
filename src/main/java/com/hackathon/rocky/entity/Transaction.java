package com.hackathon.rocky.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.hackathon.rocky.Sequence.generatorId;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;


@Data
@Entity
@Table(name = "Transaction")
@DynamicUpdate
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class Transaction implements Serializable {
    private static final long serialVersionUID = 1L;


    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trans_seq")
    @GenericGenerator(
            name = "trans_seq",
            strategy = "com.hackathon.rocky.Sequence.generatorId" +
                    "",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = generatorId.INCREMENT_PARAM, value = "50"),
                    @org.hibernate.annotations.Parameter(name = generatorId.VALUE_PREFIX_PARAMETER, value = "CPT"),
                    @org.hibernate.annotations.Parameter(name = generatorId.NUMBER_FORMAT_PARAMETER, value = "%05d")})
    @Column(name="Trans_id")
    private String Trans_id;

    @Column(name = "Trans_time")
    private LocalDateTime Trans_time = LocalDateTime.now();

    @Column(name = "Points_Affected")
    private Integer Points_affected;

    @Column(name = "Current_Point")
    private Integer Current_point;

    @Column(name = "Trans_Type")
    private String Trans_Type;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "Coupon_id")
    private Coupon coupon;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "User_id")
    private User user;



}
