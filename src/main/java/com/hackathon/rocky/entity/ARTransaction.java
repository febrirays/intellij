package com.hackathon.rocky.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.hackathon.rocky.generator.StringSequenceIdentifier;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Entity
@Table(name = "AR_Transaction")
@DynamicUpdate
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class ARTransaction{
    @Id
    @GeneratedValue(generator = "assigned-sequence", strategy = GenerationType.SEQUENCE)
    @GenericGenerator(
            name = "assigned-sequence",
            strategy = "com.hackathon.rocky.generator.StringSequenceIdentifier",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = StringSequenceIdentifier.INCREMENT_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = StringSequenceIdentifier.VALUE_PREFIX_PARAMETER, value = "AR_"),
                    @org.hibernate.annotations.Parameter(name = StringSequenceIdentifier.NUMBER_FORMAT_PARAMETER, value = "%05d")
            }
    )
    @Column(name = "AR_TRANSACTION_ID")
    private String arTransactionId;

    @Column(name = "AR_TRANSACTION_TIME")
    private LocalDateTime arTransactionTime = LocalDateTime.now();

    @Column(name = "NEWEST_RECORD")
    private boolean isNewest = true;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "LOCATION_ID")
    private ARLocation arLocation;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "User_id")
    private User user;

    public String getArTransactionId() {
        return arTransactionId;
    }

    public void setArTransactionId(String arTransactionId) {
        this.arTransactionId = arTransactionId;
    }

    public ARLocation getArLocation() {
        return arLocation;
    }

    public void setArLocation(ARLocation arLocation) {
        this.arLocation = arLocation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getArTransactionTime() {
        return arTransactionTime;
    }

    public void setArTransactionTime(LocalDateTime arTransactionTime) {
        this.arTransactionTime = arTransactionTime;
    }

    public boolean isNewest() {
        return isNewest;
    }

    public void setNewest(boolean newest) {
        isNewest = newest;
    }
}