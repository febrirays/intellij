package com.hackathon.rocky.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.hackathon.rocky.generator.StringSequenceIdentifier;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Data
@Entity
@Table(name="Users")
@DynamicUpdate
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})


public class User implements Serializable{
    private static final long serialVersionUID = 1L;




    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "user_seq")
    @GenericGenerator(name="user_seq",strategy = "com.hackathon.rocky.generator.StringSequenceIdentifier",parameters = {
            @org.hibernate.annotations.Parameter(name = StringSequenceIdentifier.INCREMENT_PARAM, value = "50"),
            @org.hibernate.annotations.Parameter(name = StringSequenceIdentifier.VALUE_PREFIX_PARAMETER, value = "UR"),
            @org.hibernate.annotations.Parameter(name = StringSequenceIdentifier.NUMBER_FORMAT_PARAMETER, value = "%05d")
    })
    @Column(name = "User_id")
    private String userId;

    @Column(name = "User_Name")
    private String userName;

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @Column(name = "password")
    private String userPassword;




    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return userPassword.equals(user.userPassword);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userPassword);
    }

    public Integer getUserPoint() {
        return userPoint;
    }

    public void setUserPoint(Integer userPoint) {
        this.userPoint = userPoint;
    }

    @Column(name = "User_Point")
    private Integer userPoint;

    //@OneToMany(mappedBy = "CouponTransaction", cascade = CascadeType.ALL)

    public User(){

    }

    public User(User user) {
        this.userName=user.getUserName();
        this.userId=user.getUserId();
        this.userPassword=user.getUserPassword();
        this.userPoint=user.getUserPoint();
    }
}
