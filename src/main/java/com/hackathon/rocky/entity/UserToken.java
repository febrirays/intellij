package com.hackathon.rocky.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hackathon.rocky.generator.StringSequenceIdentifier;
import lombok.Data;
import org.apache.tomcat.jni.Local;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;


@Data
@Entity
@Table(name="User_Token")
@DynamicUpdate
public class UserToken {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "userToken_seq")
    @GenericGenerator(name="userToken_seq",strategy = "com.hackathon.rocky.generator.StringSequenceIdentifier",parameters = {
            @org.hibernate.annotations.Parameter(name = StringSequenceIdentifier.INCREMENT_PARAM, value = "50"),
            @org.hibernate.annotations.Parameter(name = StringSequenceIdentifier.VALUE_PREFIX_PARAMETER, value = "TKP"),
            @org.hibernate.annotations.Parameter(name = StringSequenceIdentifier.NUMBER_FORMAT_PARAMETER, value = "%05d")
    })
    @Column(name = "User_Token_id")
    private String userTokenId;

    @Column(name = "Token_Type")
    private String tokenType;


    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "User_id")
    private User user;

    @Column(name = "Token_DateTime")
    private Timestamp tokenDateTime;

    public UserToken()
    {

    }

    public UserToken(String tokenType, User user, Timestamp tokenDateTime) {
        this.tokenType = tokenType;
        this.user = user;
        this.tokenDateTime = tokenDateTime;
    }

    public Timestamp getTokenDateTime() {
        return tokenDateTime;
    }

    public void setTokenDateTime(Timestamp tokenDateTime) {
        this.tokenDateTime = tokenDateTime;
    }

    public String getUserTokenId() {
        return userTokenId;
    }

    public void setUserTokenId(String userTokenId) {
        this.userTokenId = userTokenId;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
