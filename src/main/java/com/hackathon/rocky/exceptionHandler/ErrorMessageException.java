package com.hackathon.rocky.exceptionHandler;

import lombok.Getter;
import lombok.Setter;

public class ErrorMessageException extends RuntimeException {
    public ErrorMessageException(String id) {
        super(id);
    }
}
